import { Schema, model } from 'mongoose';
import conn from '../databases/init.mongodb';

interface IUser {
    Wallet: {
        balance: number;
        deposit: number;
        withdraw: number;
    };
    payment: {
        bank: number;
        roles: string;
        email: string;
        password: string;
        username: string;
    };
    contract: any;
    rank: string;
    changeBalance: number;
    uploadCCCDFont: string;
    uploadCCCDBeside: string;
    uploadLicenseFont: string;
    uploadLicenseBeside: string;
    lock: boolean;
    activated: boolean;
}

const user = new Schema<IUser>(
    {
        Wallet: {
            balance: { type: Number, default: 0.0 },
            deposit: { type: Number, default: 0.0 },
            withdraw: { type: Number, default: 0.0 }
        },
        payment: {
            bank: { type: Number, default: 0 },
            private: { type: Boolean, default: false },
            roles: {
                type: String,
                enum: ['user', 'admin', 'manager'],
                default: 'user'
            },
            email: {
                type: String,
                default: '',
                unique: true,
                required: true
            },
            password: { type: String, default: '', required: true },
            username: {
                type: String,
                default: '',
                required: true,
                unique: true
            }
        },
        contract: { type: [], default: [] },
        rank: { type: String, default: 'STANDARD' },
        changeBalance: { type: Number, default: 0.0 },
        uploadCCCDFont: { type: String, default: '' },
        uploadCCCDBeside: { type: String, default: '' },
        uploadLicenseFont: { type: String, default: '' },
        uploadLicenseBeside: { type: String, default: '' },
        lock: { type: Boolean, default: false },
        activated: { type: Boolean, default: false }
    },
    {
        timestamps: true,
        collection: 'users'
    }
);

const userModel = conn.model<IUser>('users', user);

export { userModel, IUser };
