import { DataTypes } from 'sequelize';
import sequelizeConnection from '../databases/init.sequelize';
import Deposit from './deposit.model';
import Withdraw from './withdraw.model';

const Payment = sequelizeConnection.define(
    'payment',
    {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        type_payment: {
            type: DataTypes.ENUM('admin', 'user'),
            defaultValue: 'user'
        },
        bank_name: {
            type: DataTypes.STRING
        },
        account_name: { type: DataTypes.STRING },
        account_number: { type: DataTypes.STRING, unique: true }
    },
    {
        freezeTableName: true,
        timestamps: true,
        underscored: true,
        paranoid: true,
        indexes: [
            {
                unique: true,
                fields: ['id']
            }
        ]
    }
);

export default Payment;
