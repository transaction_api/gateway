/* eslint-disable @typescript-eslint/no-explicit-any */
import amqplib from 'amqplib';

const amqp_url = `${process.env.URL_RABBITMQ}`;

const sendTelegram = async (msg: string) => {
    try {
        // 1.create connect
        const conn = await amqplib.connect(amqp_url);
        // 2.create channel
        const channel = await conn.createChannel();
        // 3.create name queue
        const nameQueue = 'telegram';
        // 4.create queue
        await channel.assertQueue(nameQueue, {
            durable: true // tinh ben bi: la khi bi tat dot ngot hay restart k mat messgae khi la true
        });
        // 5.send to queue
        await channel.sendToQueue(nameQueue, Buffer.from(msg), {
            persistent: true, // tin nhan se xu ly lien tuc se luu vao o dia hay cache neu 1 trong 2 hai hu chay cai con lai
            expiration: '10000' // TTL => Time to Life: Exist in 10 seconds
        });

        // 6.close connection
        setTimeout(async () => {
            await channel.close();
            await conn.close();
        }, 3 * 1000);
    } catch (error: any) {
        console.log(error.message);
    }
};

export default sendTelegram;
