/* eslint-disable @typescript-eslint/no-explicit-any */
import { NextFunction, Request, Response } from 'express';
import { BadRequestError } from '../core/error.response';
import { CREATED, Ok } from '../core/success.response';
import userServices from '../services/user.services';
import bcrypt from 'bcrypt';
import { saltBcrypt } from '../utils/functions.utils';
import { signAccessToken, signRefreshToken } from '../middleWares/signToken';
import redisClient from '../databases/init.redis';
import jwt from 'jsonwebtoken';

export default new (class AuthController {
    // [POST] /auth/register
    register = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const userFindByEmail = await userServices.getByEmail(
                req.body.email
            );
            const userFindByUsername = await userServices.getByUsername(
                req.body.username
            );

            if (userFindByEmail) throw new BadRequestError(`Email is exist`);
            if (userFindByUsername)
                throw new BadRequestError(`Username is exist`);
            const password = bcrypt.hashSync(req.body.password, saltBcrypt);
            const prepare = {
                'payment.email': req.body.email,
                'payment.username': req.body.username,
                'payment.password': password
            };

            return new CREATED({
                metadata: await userServices.Create(prepare)
            }).send(res);
        } catch (error: any) {
            next(error);
        }
    };

    // [POST] /auth/register/admin
    registerAdmin = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const userFindByEmail = await userServices.getByEmail(
                req.body.email
            );
            const userFindByUsername = await userServices.getByUsername(
                req.body.username
            );

            if (userFindByEmail) throw new BadRequestError(`Email is exist`);
            if (userFindByUsername)
                throw new BadRequestError(`Username is exist`);
            const password = bcrypt.hashSync(req.body.password, saltBcrypt);
            const prepare = {
                'payment.email': req.body.email,
                'payment.username': req.body.username,
                'payment.password': password,
                'payment.roles': 'admin'
            };

            return new CREATED({
                metadata: await userServices.Create(prepare)
            }).send(res);
        } catch (error: any) {
            next(error);
        }
    };

    // [POST] /auth/login
    login = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const { username, password } = req.body;
            let user: any;
            const isUsername: any = await userServices.getByUsername(username);
            const isEmail: any = await userServices.getByEmail(username);
            if (isUsername) {
                await userServices.checkPassword(
                    isUsername.payment.password,
                    password
                );
                user = isUsername;
            }

            if (isEmail) {
                await userServices.checkPassword(
                    isEmail.payment.password,
                    password
                );
                user = isEmail;
            }
            if (!user) throw new BadRequestError(`User is not exist`);
            const accessToken = signAccessToken(user._id);
            const refreshToken = signRefreshToken(user._id);
            redisClient.set(`${user._id}_access_token`, accessToken, {
                EX: 30 * 60
            });
            redisClient.set(`${user._id}_refresh_token`, refreshToken, {
                EX: 200 * 24 * 60 * 60
            });

            return new Ok({
                metadata: {
                    user,
                    token: accessToken
                }
            }).send(res);
        } catch (error: any) {
            next(error);
        }
    };

    // [POST] /auth/token/refresh/:email
    refreshToken = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const { email } = req.params;
            const user: any = await userServices.getByEmail(email);
            if (!user) throw new BadRequestError(`User is not exist`);
            const refreshTokenFind = await redisClient.get(
                `${user._id}_refresh_token`
            );
            if (!refreshTokenFind) {
                throw new BadRequestError(
                    `Refresh token is not valid or not true. Please login again!`
                );
            }
            jwt.verify(
                refreshTokenFind,
                `${process.env.REFRESH_TOKEN_JWT_SECRET}`,
                async (err: any, decoded: any) => {
                    if (err) throw new BadRequestError(err.message);
                    const newToken = signAccessToken(decoded.uid);
                    redisClient.set(`${decoded.uid}_access_token`, newToken, {
                        EX: 30 * 60
                    });
                    return new Ok({ metadata: newToken }).send(res);
                }
            );
        } catch (error: any) {
            next(error);
        }
    };

    // [GET] /auth/logout/:email
    logout = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const { email } = req.params;
            const user: any = await userServices.getByEmail(email);
            if (!user) throw new BadRequestError(`User is not exist`);
            redisClient.del(`${user._id}_access_token`);
            redisClient.del(`${user._id}_refresh_token`);
            return new Ok({ message: 'Logout successfully' }).send(res);
        } catch (error) {
            next(error);
        }
    };
})();
