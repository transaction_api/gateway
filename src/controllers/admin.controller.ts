import { NextFunction, Request, Response } from 'express';
import { Ok } from '../core/success.response';
import contractServices from '../services/contract.services';
import depositServices from '../services/deposit.services';
import withdrawServices from '../services/withdraw.services';
import userServices from '../services/user.services';
import { CONTRACT_TYPE } from '../types/contract.types';
import {
    CONTRACT_ENUM_STATUS,
    CONTRACT_ENUM_STATUS_TYPE
} from '../enum/contract.enum';
import Path from 'path';
import { rename_file } from '../utils/functions.utils';
import rateServices from '../services/rate.services';
import paymentServices from '../services/payment.services';
import { BadRequestError } from '../core/error.response';
import { DEPOSIT_ENUM_STATUS } from '../enum/deposit.enum';
import { WITHDRAW_ENUM_STATUS } from '../enum/withdraw.enum';

export default new (class AdminController {
    // [PUT] /admin/handle/contract/:idContract
    handleContract = async (
        req: Request,
        res: Response,
        next: NextFunction
    ) => {
        try {
            const { status } = req.body;
            const { idContract } = req.params;

            return new Ok({
                message: await contractServices.handleContract(
                    idContract,
                    status
                )
            }).send(res);
        } catch (error) {
            next(error);
        }
    };

    // [PUT] /admin/handle/deposit/:idDeposit
    handleDeposit = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const { idDeposit } = req.params;
            const { status } = req.body;

            return new Ok({
                message: await depositServices.handleDeposit(idDeposit, status)
            }).send(res);
        } catch (error) {
            next(error);
        }
    };

    // [PUT] /admin/handle/withdraw/:idWithdraw
    handleWithdraw = async (
        req: Request,
        res: Response,
        next: NextFunction
    ) => {
        try {
            const { idWithdraw } = req.params;
            const { status } = req.body;

            return new Ok({
                message: await withdrawServices.handleWithdraw(
                    idWithdraw,
                    status
                )
            }).send(res);
        } catch (error) {
            next(error);
        }
    };

    // [GET] /admin/deposit/:idDeposit
    getDeposit = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const { idDeposit } = req.params;
            return new Ok({
                metadata: (await depositServices.getById(idDeposit)) || {}
            }).send(res);
        } catch (error) {
            next(error);
        }
    };

    // [GET] /admin/deposit
    getAllDeposits = async (
        req: Request,
        res: Response,
        next: NextFunction
    ) => {
        try {
            return new Ok({
                metadata: await depositServices.getAll()
            }).send(res);
        } catch (error) {
            next(error);
        }
    };

    // [PUT] /admin/deposit/:idDeposit
    updateDeposit = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const { idDeposit } = req.params;
            return new Ok({
                message: await depositServices.Update(idDeposit, req.body)
            }).send(res);
        } catch (error) {
            next(error);
        }
    };

    // [DELETE] /admin/deposit/:idDeposit
    deleteDeposit = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const { idDeposit } = req.params;
            const deposit = await depositServices.getById(idDeposit);
            if (!deposit)
                throw new BadRequestError(`Yêu cầu nạp tiền không tồn tại`);
            await depositServices.handleDeposit(
                idDeposit,
                DEPOSIT_ENUM_STATUS.Canceled
            );
            return new Ok({
                message: await depositServices.Delete(idDeposit)
            }).send(res);
        } catch (error) {
            next(error);
        }
    };

    // [GET] /admin/withdraw/:idWithdraw
    getWithdraw = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const { idWithdraw } = req.params;
            return new Ok({
                metadata: (await withdrawServices.getById(idWithdraw)) || {}
            }).send(res);
        } catch (error) {
            next(error);
        }
    };

    // [GET] /admin/withdraw
    getAllWithdraws = async (
        req: Request,
        res: Response,
        next: NextFunction
    ) => {
        try {
            return new Ok({
                metadata: await withdrawServices.getAll()
            }).send(res);
        } catch (error) {
            next(error);
        }
    };

    // [PUT] /admin/withdraw/:idWithdraw
    updateWithdraw = async (
        req: Request,
        res: Response,
        next: NextFunction
    ) => {
        try {
            const { idWithdraw } = req.params;
            return new Ok({
                message: await withdrawServices.Update(idWithdraw, req.body)
            }).send(res);
        } catch (error) {
            next(error);
        }
    };

    // [DELETE] /admin/withdraw/:idWithdraw
    deleteWithdraw = async (
        req: Request,
        res: Response,
        next: NextFunction
    ) => {
        try {
            const { idWithdraw } = req.params;
            const withdraw = await withdrawServices.getById(idWithdraw);
            if (!withdraw)
                throw new BadRequestError(`Yêu cầu rút tiền không tồn tại`);
            await withdrawServices.handleWithdraw(
                idWithdraw,
                WITHDRAW_ENUM_STATUS.Canceled
            );
            return new Ok({
                message: await withdrawServices.Delete(idWithdraw)
            }).send(res);
        } catch (error) {
            next(error);
        }
    };

    // [GET] /admin/user/:idUser
    getUser = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const { idUser } = req.params;
            return new Ok({
                metadata: (await userServices.getById(idUser)) || {}
            }).send(res);
        } catch (error) {
            next(error);
        }
    };

    // [GET] /admin/user
    getAllUsers = async (req: Request, res: Response, next: NextFunction) => {
        try {
            return new Ok({
                metadata: await userServices.getAll()
            }).send(res);
        } catch (error) {
            next(error);
        }
    };

    // [PUT] /admin/user/:idUser
    updateUser = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const { idUser } = req.params;
            return new Ok({
                message: await userServices.Update(idUser, req.body)
            }).send(res);
        } catch (error) {
            next(error);
        }
    };

    // [DELETE] /admin/user/:idUser
    deleteUser = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const { idUser } = req.params;
            return new Ok({
                message: await userServices.Delete(idUser)
            }).send(res);
        } catch (error) {
            next(error);
        }
    };

    // [GET] /admin/contract/:idContract
    getContract = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const { idContract } = req.params;
            return new Ok({
                metadata: (await contractServices.getById(idContract)) || {}
            }).send(res);
        } catch (error) {
            next(error);
        }
    };

    // [GET] /admin/contract
    getAllContracts = async (
        req: Request,
        res: Response,
        next: NextFunction
    ) => {
        try {
            const contracts: Array<any> = await contractServices.getAll();
            const USDContract = contracts.filter(
                (contract: CONTRACT_TYPE) =>
                    contract.type === CONTRACT_ENUM_STATUS_TYPE.USD
            );
            const AGRICULTUREContract = contracts.filter(
                (contract: CONTRACT_TYPE) =>
                    contract.type === CONTRACT_ENUM_STATUS_TYPE.AGRICULTURE
            );
            return new Ok({
                metadata: {
                    USDContract,
                    AGRICULTUREContract
                }
            }).send(res);
        } catch (error) {
            next(error);
        }
    };

    // [PUT] /admin/contract/image/:idContract
    addImageContract = async (
        req: Request,
        res: Response,
        next: NextFunction
    ) => {
        try {
            const { idContract } = req.params;
            const imageFile: any = req.file;
            const date = Date.now();
            const contract = await contractServices.getById(idContract);
            if (!contract) throw new BadRequestError('Hợp đồng không tồn tại');
            const oldPathImage = imageFile.path;
            const nameFile = `${date}-${imageFile.originalname}`;
            const newPathImage = Path.join(imageFile.destination, nameFile);
            await rename_file(oldPathImage, newPathImage);
            const pathImageContract = Path.join('/images', nameFile);
            return new Ok({
                message: await contractServices.Update(idContract, {
                    statement: pathImageContract
                })
            }).send(res);
        } catch (error) {
            next(error);
        }
    };

    // [PUT] /admin/contract/:idContract
    updateContract = async (
        req: Request,
        res: Response,
        next: NextFunction
    ) => {
        try {
            const { idContract } = req.params;
            return new Ok({
                message: await contractServices.Update(idContract, req.body)
            }).send(res);
        } catch (error) {
            next(error);
        }
    };

    // [DELETE] /admin/contract/:idContract
    deleteContract = async (
        req: Request,
        res: Response,
        next: NextFunction
    ) => {
        try {
            const { idContract } = req.params;
            await contractServices.handleContract(
                idContract,
                CONTRACT_ENUM_STATUS.Canceled
            );
            return new Ok({
                message: await contractServices.Delete(idContract)
            }).send(res);
        } catch (error) {
            next(error);
        }
    };

    // [PUT] /admin/user/role/:idUser
    changeRole = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const { idUser } = req.params;
            return new Ok({
                message: await userServices.changeRole(idUser, req.params.role)
            }).send(res);
        } catch (error) {
            next(error);
        }
    };

    // [GET] /admin/rate
    getRate = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const rates: Array<any> = await rateServices.getAll();
            return new Ok({
                metadata: rates[0] ? rates[0] : {}
            }).send(res);
        } catch (error) {
            next(error);
        }
    };

    // [POST] /admin/rate
    createRate = async (req: Request, res: Response, next: NextFunction) => {
        try {
            return new Ok({
                metadata: await rateServices.Create(req.body)
            }).send(res);
        } catch (error) {
            next(error);
        }
    };

    // [PUT] /admin/rate/:idRate
    updateRate = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const { idRate } = req.params;
            return new Ok({
                message: await rateServices.Update(idRate, req.body)
            }).send(res);
        } catch (error) {
            next(error);
        }
    };

    // [DELETE] /admin/rate/:idRate
    deleteRate = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const { idRate } = req.params;
            return new Ok({
                message: await rateServices.Delete(idRate)
            }).send(res);
        } catch (error) {
            next(error);
        }
    };

    // [GET] /admin/payment
    getAllPayments = async (
        req: Request,
        res: Response,
        next: NextFunction
    ) => {
        try {
            return new Ok({ metadata: await paymentServices.getAll() }).send(
                res
            );
        } catch (error: any) {
            next(error);
        }
    };

    // [GET] /admin/payment/:idPayment
    getPaymentById = async (
        req: Request,
        res: Response,
        next: NextFunction
    ) => {
        try {
            const { idPayment } = req.params;
            return new Ok({
                metadata: (await paymentServices.getById(idPayment)) || {}
            }).send(res);
        } catch (error: any) {
            next(error);
        }
    };

    // [GET] /admin/payments
    getAllPaymentByType = async (
        req: Request,
        res: Response,
        next: NextFunction
    ) => {
        try {
            const payments = await paymentServices.getAll();
            const admin = payments.filter(
                (payment: any) => payment.type_payment === 'admin'
            );
            const user = payments.filter(
                (payment: any) => payment.type_payment === 'user'
            );
            return new Ok({
                metadata: {
                    admin: admin,
                    user: user
                }
            }).send(res);
        } catch (error: any) {
            next(error);
        }
    };

    // [POST] /admin/payment
    createPayment = async (req: Request, res: Response, next: NextFunction) => {
        try {
            return new Ok({
                metadata: await paymentServices.Create(req.body)
            }).send(res);
        } catch (error: any) {
            next(error);
        }
    };

    // [PUT] /admin/payment/:idPayment
    updatePayment = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const { idPayment } = req.params;
            return new Ok({
                message: await paymentServices.Update(idPayment, req.body)
            }).send(res);
        } catch (error: any) {
            next(error);
        }
    };

    // [DELETE] /admin/payment/:idPayment
    deletePayment = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const { idPayment } = req.params;
            return new Ok({
                message: await paymentServices.Delete(idPayment)
            }).send(res);
        } catch (error: any) {
            next(error);
        }
    };

    // --------------------------------------------------[PAGING]--------------------------------------------------

    // [GET] /admin/paging/user
    pagingUser = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const { page, show, search } = req.query;
            const pages = parseInt(`${page}`) || 1;
            const shows = parseInt(`${show}`) || 10;
            const users = await userServices.getAll();
            const searches = `${search}` || '';
            if (!search) {
                return new Ok({
                    metadata: {
                        page: pages,
                        show: shows,
                        total: users.length,
                        users: await userServices.pagingSearch(
                            parseInt(`${pages}`),
                            parseInt(`${shows}`)
                        )
                    }
                }).send(res);
            } else {
                return new Ok({
                    metadata: {
                        page: pages,
                        show: shows,
                        total: users.length,
                        users: await userServices.pagingSearch(
                            parseInt(`${pages}`),
                            parseInt(`${shows}`),
                            searches.toLowerCase()
                        )
                    }
                }).send(res);
            }
        } catch (error: any) {
            next(error);
        }
    };

    // [GET] /admin/paging/deposit
    pagingDeposit = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const { page, show, search } = req.query;
            const pages = parseInt(`${page}`) || 1;
            const shows = parseInt(`${show}`) || 10;
            const searches = `${search}` || '';
            if (!search) {
                return new Ok({
                    metadata: await depositServices.pagingSearch(
                        parseInt(`${pages}`),
                        parseInt(`${shows}`)
                    )
                }).send(res);
            } else {
                return new Ok({
                    metadata: await depositServices.pagingSearch(
                        parseInt(`${pages}`),
                        parseInt(`${shows}`),
                        searches.toLowerCase()
                    )
                }).send(res);
            }
        } catch (error: any) {
            next(error);
        }
    };

    // [GET] /admin/paging/withdraw
    pagingWithdraw = async (
        req: Request,
        res: Response,
        next: NextFunction
    ) => {
        try {
            const { page, show, search } = req.query;
            const pages = parseInt(`${page}`) || 1;
            const shows = parseInt(`${show}`) || 10;
            const searches = `${search}` || '';
            if (!search) {
                return new Ok({
                    metadata: await withdrawServices.pagingSearch(
                        parseInt(`${pages}`),
                        parseInt(`${shows}`)
                    )
                }).send(res);
            } else {
                return new Ok({
                    metadata: await withdrawServices.pagingSearch(
                        parseInt(`${pages}`),
                        parseInt(`${shows}`),
                        searches.toLowerCase()
                    )
                }).send(res);
            }
        } catch (error: any) {
            next(error);
        }
    };

    // --------------------------------------------------[PAGING]--------------------------------------------------

    // --------------------------------------------------[TOTAL]--------------------------------------------------

    // [GET] /total
    totalDashboard = async (
        req: Request,
        res: Response,
        next: NextFunction
    ) => {
        try {
            return new Ok({
                metadata: {
                    totalDeposit: await depositServices.totalDeposit(),
                    totalWithdraw: await withdrawServices.totalWithdraw(),
                    userHad: await userServices.userHadBalance()
                }
            }).send(res);
        } catch (error: any) {
            next(error);
        }
    };

    // --------------------------------------------------[TOTAL]--------------------------------------------------
})();
