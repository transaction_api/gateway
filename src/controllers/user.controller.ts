/* eslint-disable @typescript-eslint/no-explicit-any */
import { NextFunction, Request, Response } from 'express';
import userServices from '../services/user.services';
import { BadRequestError } from '../core/error.response';
import paymentServices from '../services/payment.services';
import { CREATED, Ok } from '../core/success.response';
import depositServices from '../services/deposit.services';
import {
    precisionRound,
    rename_file,
    restore_image_base_64
} from '../utils/functions.utils';
import Path from 'path';
import withdrawServices from '../services/withdraw.services';
import otpServices from '../services/otp.services';
import { WITHDRAW_ENUM_STATUS } from '../enum/withdraw.enum';
import otpGenerator from 'otp-generator';
import {
    CONTRACT_ENUM_STATUS,
    CONTRACT_ENUM_STATUS_TYPE
} from '../enum/contract.enum';
import { CONTRACT_TYPE } from '../types/contract.types';
import contractServices from '../services/contract.services';
import sendContract from '../queues/sendContract';
import sendMail from '../queues/sendMail';
import sendTelegram from '../queues/sendTelegram';

export default new (class UserController {
    // [PUT] /users/payment/:idUser
    addPayment = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const { idUser } = req.params;
            const user: any = await userServices.getById(idUser);
            if (!user) throw new BadRequestError(`User is not exist`);

            if (user.payment.bank !== 0)
                throw new BadRequestError(`User had payment bank`);

            let payment: any;

            const checkPayment = await paymentServices.getByAccountNumber(
                req.body.accountNumber,
                req.body.bankName,
                req.body.accountName
            );
            if (checkPayment) {
                payment = checkPayment;
            } else {
                payment = await paymentServices.Create({
                    bank_name: req.body.bankName,
                    account_name: req.body.accountName,
                    account_number: req.body.accountNumber
                });
            }

            return new Ok({
                message: await userServices.Update(idUser, {
                    'payment.bank': payment.id
                })
            }).send(res);
        } catch (error) {
            next(error);
        }
    };

    // [POST] /users/deposit/:idUser
    deposit = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const { idUser } = req.params;
            const { amount, idPayment } = req.body;
            const user: any = await userServices.getById(idUser);
            const paymentAdmin: any = await paymentServices.getById(idPayment);
            if (!user) throw new BadRequestError(`User is not exist`);
            if (!paymentAdmin)
                throw new BadRequestError(`Payment admin is not exist.`);
            if (paymentAdmin.type_payment !== 'admin')
                throw new BadRequestError(`Payment is must be admin`);

            return new CREATED({
                metadata: await depositServices.Create({
                    userId: idUser,
                    amount,
                    idPayment: idPayment,
                    note: 'Not has statement image'
                })
            }).send(res);
        } catch (error: any) {
            next(error);
        }
    };

    // [PUT] /users/deposit/image/:idDeposit
    addImageDeposit = async (
        req: Request,
        res: Response,
        next: NextFunction
    ) => {
        try {
            const { idDeposit } = req.params;
            const { image, imageName } = req.body;
            const imageFile: any = req.file;
            const date = Date.now();
            const deposit: any = await depositServices.getDepositAndPayment(
                idDeposit
            );
            if (!deposit) throw new BadRequestError(`Deposit is not exist`);
            // console.log(deposit);
            if (image && imageName) {
                const name_file = `${date}-${imageName}`;
                await restore_image_base_64(image, name_file, 'images');
                const pathImageDeposit = Path.join('/images', name_file);
                sendTelegram(
                    JSON.stringify({
                        type: 'sendDeposit',
                        data: deposit
                    })
                );
                return new Ok({
                    message: await depositServices.Update(idDeposit, {
                        statement: pathImageDeposit,
                        note: ''
                    })
                }).send(res);
            } else {
                const oldPathImage = imageFile.path;
                const nameFile = `${date}-${imageFile.originalname}`;
                const newPathImage = Path.join(imageFile.destination, nameFile);
                await rename_file(oldPathImage, newPathImage);
                const pathImageDeposit = Path.join('/images', nameFile);
                sendTelegram(
                    JSON.stringify({
                        type: 'sendDeposit',
                        data: deposit
                    })
                );
                return new Ok({
                    message: await depositServices.Update(idDeposit, {
                        statement: pathImageDeposit,
                        note: ''
                    })
                }).send(res);
            }
        } catch (error: any) {
            next(error);
        }
    };

    // [POST] /users/withdraw/:idUser
    withdraw = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const { idUser } = req.params;
            const { amount } = req.body;
            const user: any = await userServices.getById(idUser);
            if (!user)
                throw new BadRequestError(
                    `User is not exist. Please addition.`
                );
            if (user.payment.bank === 0)
                throw new BadRequestError(`Payment of user is not exist.`);

            const otp = otpGenerator.generate(4, {
                upperCaseAlphabets: false,
                specialChars: false,
                lowerCaseAlphabets: false
            });
            sendMail(
                JSON.stringify({
                    type: 'sendOTPWithdraw',
                    data: {
                        otp,
                        user
                    }
                })
            );
            const withdraw: any = await withdrawServices.Create({
                userId: idUser,
                amount,
                idPayment: user.payment.bank
            });
            await otpServices.Create({
                idUser,
                code: otp,
                type: 'otp_withdraw',
                idServices: withdraw.id
            });
            return new CREATED({
                metadata: {
                    withdraw
                }
            }).send(res);
        } catch (error: any) {
            next(error);
        }
    };

    // [PUT] /users/withdraw/otp
    otpWithdraw = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const { otp } = req.body;
            const OTP: any = await otpServices.getByOTP(otp);

            if (!OTP) throw new BadRequestError(`OTP: ${otp} is not exist`);
            if (OTP.type !== 'otp_withdraw')
                throw new BadRequestError(`OTP: ${otp} is not exist`);
            const { idServices } = OTP;
            const withdraw = await withdrawServices.getWithdrawAndPayment(
                idServices
            );
            if (!withdraw)
                throw new BadRequestError(`Yêu cầu rút tiền không tồn tại.`);
            const result = await withdrawServices.handleWithdraw(
                idServices,
                WITHDRAW_ENUM_STATUS.Confirmed
            );
            await otpServices.Delete(OTP._id);
            sendTelegram(
                JSON.stringify({
                    type: 'sendWithdraw',
                    data: withdraw
                })
            );
            return new Ok({
                message: result
            }).send(res);
        } catch (error: any) {
            next(error);
        }
    };

    // [GET] /users/withdraw/otp/resend/:idUser/:idWithdraw
    resendOtpWithdraw = async (
        req: Request,
        res: Response,
        next: NextFunction
    ) => {
        try {
            const { idUser, idWithdraw } = req.params;
            const otpByUser: Array<any> = await otpServices.getByIdUser(idUser);
            if (otpByUser.length > 0) {
                otpByUser.forEach(async (otp: any) => {
                    await otpServices.Delete(otp._id);
                });
            }
            const otp = otpGenerator.generate(4, {
                upperCaseAlphabets: false,
                specialChars: false,
                lowerCaseAlphabets: false
            });
            const user = await userServices.getById(idUser);
            if (!user) throw new BadRequestError(`User is not exist`);
            sendMail(
                JSON.stringify({
                    type: 'sendOTPWithdraw',
                    data: {
                        otp,
                        user
                    }
                })
            );
            await otpServices.Create({
                idUser,
                code: otp,
                type: 'otp_withdraw',
                idServices: idWithdraw
            });
            return new CREATED({ metadata: otp }).send(res);
        } catch (error) {
            next(error);
        }
    };

    // [POST] /users/withdraw/cancel/:idWithdraw
    cancelWithdraw = async (
        req: Request,
        res: Response,
        next: NextFunction
    ) => {
        try {
            const { idWithdraw } = req.params;
            return new Ok({
                message: await withdrawServices.Delete(idWithdraw)
            }).send(res);
        } catch (error) {
            next(error);
        }
    };

    // [POST] /users/contract/:idUser
    contract = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const { idUser } = req.params;
            const input_data: CONTRACT_TYPE = {
                userId: idUser,
                status: CONTRACT_ENUM_STATUS.Pending,
                rate: 0.06,
                principal: parseFloat(req.body?.principal),
                interest_rate: 0,
                cycle: parseInt(req.body?.cycle), // đơn vị tính bằng tháng
                number_of_days_taken: 0,
                type: req.body?.type,
                statement: '',
                date_start: req.body.day
            };

            const user = await userServices.getById(idUser);
            if (!user) throw new BadRequestError('User is not exist');
            const contract = await contractServices.Create(input_data);

            // rabbitmq to confirm contract
            sendContract(
                JSON.stringify({
                    type: 'confirm_contract',
                    data: contract
                })
            );
            sendTelegram(
                JSON.stringify({
                    type: 'sendContract',
                    data: contract
                })
            );

            return new CREATED({
                metadata: contract
            }).send(res);
        } catch (error) {
            next(error);
        }
    };

    // [GET] /users/contract/destroy/:idContract
    destroyContract = async (
        req: Request,
        res: Response,
        next: NextFunction
    ) => {
        try {
            const { idContract } = req.params;
            return new Ok({
                message: await contractServices.Delete(idContract)
            }).send(res);
        } catch (error) {
            next(error);
        }
    };

    // [GET] /users/contract/disbursement/:idContract
    getDisbursement = async (
        req: Request,
        res: Response,
        next: NextFunction
    ) => {
        try {
            const { idContract } = req.params;
            return new Ok({
                metadata: {
                    id: idContract,
                    total: await contractServices.getDisbursement(idContract)
                }
            }).send(res);
        } catch (error) {
            next(error);
        }
    };

    // [POST] /users/contract/disbursement/filed
    getDisbursementByFiled = async (
        req: Request,
        res: Response,
        next: NextFunction
    ) => {
        try {
            const { cycle, principal, type } = req.body;

            if (type === CONTRACT_ENUM_STATUS_TYPE.USD) {
                return new Ok({
                    metadata: await contractServices.getDisbursementUSD({
                        principal,
                        cycle
                    })
                }).send(res);
            } else {
                return new Ok({
                    metadata: await contractServices.getDisbursementAgriculture(
                        {
                            principal,
                            cycle
                        }
                    )
                }).send(res);
            }
        } catch (error) {
            next(error);
        }
    };

    // [GET] /users/contract/:idUser
    getAllContract = async (
        req: Request,
        res: Response,
        next: NextFunction
    ) => {
        try {
            const { idUser } = req.params;

            const contracts: Array<any> = await contractServices.getAll();

            const contractsUSD = contracts.filter(
                (contract: CONTRACT_TYPE) =>
                    contract.type === CONTRACT_ENUM_STATUS_TYPE.USD &&
                    contract.userId === idUser
            );
            const contractsAGRICULTURE = contracts.filter(
                (contract: CONTRACT_TYPE) =>
                    contract.type === CONTRACT_ENUM_STATUS_TYPE.AGRICULTURE &&
                    contract.userId === idUser
            );
            return new Ok({
                metadata: {
                    contractsUSD,
                    contractsAGRICULTURE
                }
            }).send(res);
        } catch (error) {
            next(error);
        }
    };

    // [PUT] /user/image/:idUser
    updateImageUser = async (
        req: Request,
        res: Response,
        next: NextFunction
    ) => {
        try {
            const { idUser } = req.params;
            const { image1, image2, image3, image4 } = req.body;
            const images: any = req.files;
            const date = Date.now();
            const base_fir = 'images_user';
            if (image1 && image2 && image3 && image4) {
                await restore_image_base_64(
                    image1.base64,
                    `${date}-${image1.name}`,
                    base_fir
                );
                await restore_image_base_64(
                    image2.base64,
                    `${date}-${image2.name}`,
                    base_fir
                );
                await restore_image_base_64(
                    image3.base64,
                    `${date}-${image3.name}`,
                    base_fir
                );
                await restore_image_base_64(
                    image4.base64,
                    `${date}-${image4.name}`,
                    base_fir
                );

                const image1_path = Path.join(
                    base_fir,
                    `${date}-${image1.name}`
                );
                const image2_path = Path.join(
                    base_fir,
                    `${date}-${image2.name}`
                );
                const image3_path = Path.join(
                    base_fir,
                    `${date}-${image3.name}`
                );
                const image4_path = Path.join(
                    base_fir,
                    `${date}-${image4.name}`
                );

                return new Ok({
                    metadata: await userServices.Update(idUser, {
                        uploadCCCDFont: image1_path,
                        uploadCCCDBeside: image2_path,
                        uploadLicenseFont: image3_path,
                        uploadLicenseBeside: image4_path
                    })
                }).send(res);
            } else if (images) {
                const { cccdFont, cccdBeside, licenseFont, licenseBeside } =
                    images;
                const oldPathCCCDFont = cccdFont[0].path;
                const oldPathCCCDBeside = cccdBeside[0].path;
                const oldPathLicenseFont = licenseFont[0].path;
                const oldPathLicenseBeside = licenseBeside[0].path;

                const nameFileCCCDFONT = `${date}-${cccdFont[0].originalname}`;
                const nameFileCCCDBESIDE = `${date}-${cccdBeside[0].originalname}`;
                const nameFileLicenseFont = `${date}-${licenseFont[0].originalname}`;
                const nameFileLicenseBeside = `${date}-${licenseBeside[0].originalname}`;

                const newPathCCCDFONT = Path.join(
                    cccdFont[0].destination,
                    nameFileCCCDFONT
                );
                const newPathCCCDBESIDE = Path.join(
                    cccdBeside[0].destination,
                    nameFileCCCDBESIDE
                );
                const newPathLiscenseFONT = Path.join(
                    licenseFont[0].destination,
                    nameFileLicenseFont
                );
                const newPathLiscenseBESIDE = Path.join(
                    licenseBeside[0].destination,
                    nameFileLicenseBeside
                );

                const pathImage1 = Path.join('/images_user', nameFileCCCDFONT);
                const pathImage2 = Path.join(
                    '/images_user',
                    nameFileCCCDBESIDE
                );
                const pathImage3 = Path.join(
                    '/images_user',
                    nameFileLicenseFont
                );
                const pathImage4 = Path.join(
                    '/images_user',
                    nameFileLicenseBeside
                );

                await rename_file(oldPathCCCDFont, newPathCCCDFONT);
                await rename_file(oldPathCCCDBeside, newPathCCCDBESIDE);
                await rename_file(oldPathLicenseFont, newPathLiscenseFONT);
                await rename_file(oldPathLicenseBeside, newPathLiscenseBESIDE);

                return new Ok({
                    metadata: await userServices.Update(idUser, {
                        uploadCCCDFont: pathImage1,
                        uploadCCCDBeside: pathImage2,
                        uploadLicenseFont: pathImage3,
                        uploadLicenseBeside: pathImage4
                    })
                }).send(res);
            } else {
                throw new BadRequestError('Method is not support');
            }
        } catch (error) {
            next(error);
        }
    };

    // [GET] /users/forgot/password/:email
    forgotPassword = async (
        req: Request,
        res: Response,
        next: NextFunction
    ) => {
        try {
            const { email } = req.params;
            return new Ok({
                message: await userServices.forgotPassword(email)
            }).send(res);
        } catch (error) {
            next(error);
        }
    };

    // [GET] /users/dashboard/:idUser
    DashBoard = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const { idUser } = req.params;

            const contracts: Array<any> = await contractServices.getAll();

            const contractsUSD = contracts
                .filter(
                    (contract: CONTRACT_TYPE) =>
                        contract.type === CONTRACT_ENUM_STATUS_TYPE.USD &&
                        contract.userId === idUser &&
                        (contract.status === CONTRACT_ENUM_STATUS.Completed ||
                            contract.status === CONTRACT_ENUM_STATUS.Confirmed)
                )
                .reduce(
                    (total: number, contract: CONTRACT_TYPE) =>
                        contract.status === CONTRACT_ENUM_STATUS.Confirmed
                            ? total + contract.principal
                            : total +
                              contract.principal +
                              contract.interest_rate,
                    0
                );
            const contractsAGRICULTURE = contracts
                .filter(
                    (contract: CONTRACT_TYPE) =>
                        contract.type ===
                            CONTRACT_ENUM_STATUS_TYPE.AGRICULTURE &&
                        contract.userId === idUser &&
                        (contract.status === CONTRACT_ENUM_STATUS.Completed ||
                            contract.status === CONTRACT_ENUM_STATUS.Confirmed)
                )
                .reduce(
                    (total: number, contract: CONTRACT_TYPE) =>
                        contract.status === CONTRACT_ENUM_STATUS.Confirmed
                            ? total + contract.principal
                            : total +
                              contract.principal +
                              contract.interest_rate,
                    0
                );

            const user: any = await userServices.getById(idUser);

            const totalContract = precisionRound(
                contractsUSD + contractsAGRICULTURE + user.Wallet.balance
            );

            return new Ok({
                metadata: {
                    surplus: user.Wallet.balance,
                    fundWallet: totalContract,
                    contractsUSD,
                    contractsAGRICULTURE
                }
            }).send(res);
        } catch (error: any) {
            next(error);
        }
    };
})();
