/* eslint-disable @typescript-eslint/no-explicit-any */
import { BadRequestError } from '../core/error.response';
import {
    CONTRACT_ENUM_STATUS,
    CONTRACT_ENUM_STATUS_TYPE
} from '../enum/contract.enum';
import Contract from '../models/contract.model';
import sendMail from '../queues/sendMail';
import { precisionRound } from '../utils/functions.utils';
import BaseServices from './base.services';
import userServices from './user.services';

export default new (class ContractServices implements BaseServices {
    async Create(data: any) {
        return await Contract.create(data);
    }
    async Update(id: any, data: any) {
        await Contract.update(data, { where: { id: id } });
        return 'Cập nhật hợp đồng thành công idContract = ' + id;
    }
    async Delete(id: any) {
        await Contract.destroy({ where: { id: id } });
        return 'Xoá hợp đồng thành công idContract = ' + id;
    }
    async getById(id: any) {
        return await Contract.findByPk(id);
    }
    async getAll() {
        return await Contract.findAll();
    }
    async getDisbursementUSD(contract: any) {
        const cycle_for_months = parseInt(contract.cycle);
        let LT = 0.7;
        if (contract.principal < 25000000) {
            if (cycle_for_months < 3) {
                LT = precisionRound(LT + 0);
            } else if (cycle_for_months < 6 && cycle_for_months >= 3) {
                LT = precisionRound(LT + 0.1);
            } else if (cycle_for_months < 9 && cycle_for_months >= 6) {
                LT = precisionRound(LT + 0.2);
            } else if (cycle_for_months < 12 && cycle_for_months >= 9) {
                LT = precisionRound(LT + 0.3);
            } else if (cycle_for_months < 18 && cycle_for_months >= 12) {
                LT = precisionRound(LT + 0.4);
            } else {
                LT = precisionRound(LT + 0.5);
            }
        } else if (
            contract.principal >= 25000000 &&
            contract.principal < 200000000
        ) {
            if (cycle_for_months < 3) {
                LT = precisionRound(LT + 0.1);
            } else if (cycle_for_months < 6 && cycle_for_months >= 3) {
                LT = precisionRound(LT + 0.1 + 0.1);
            } else if (cycle_for_months < 9 && cycle_for_months >= 6) {
                LT = precisionRound(LT + 0.2 + 0.1);
            } else if (cycle_for_months < 12 && cycle_for_months >= 9) {
                LT = precisionRound(LT + 0.3 + 0.1);
            } else if (cycle_for_months < 18 && cycle_for_months >= 12) {
                LT = precisionRound(LT + 0.4 + 0.1);
            } else {
                LT = precisionRound(LT + 0.5 + 0.1);
            }
        } else if (
            contract.principal >= 200000000 &&
            contract.principal < 500000000
        ) {
            if (cycle_for_months < 3) {
                LT = precisionRound(LT + 0.2);
            } else if (cycle_for_months < 6 && cycle_for_months >= 3) {
                LT = precisionRound(LT + 0.1 + 0.2);
            } else if (cycle_for_months < 9 && cycle_for_months >= 6) {
                LT = precisionRound(LT + 0.2 + 0.2);
            } else if (cycle_for_months < 12 && cycle_for_months >= 9) {
                LT = precisionRound(LT + 0.3 + 0.2);
            } else if (cycle_for_months < 18 && cycle_for_months >= 12) {
                LT = precisionRound(LT + 0.4 + 0.2);
            } else {
                LT = precisionRound(LT + 0.5 + 0.2);
            }
        } else if (
            contract.principal >= 500000000 &&
            contract.principal < 1000000000
        ) {
            if (cycle_for_months < 3) {
                LT = precisionRound(LT + 0.3);
            } else if (cycle_for_months < 6 && cycle_for_months >= 3) {
                LT = precisionRound(LT + 0.1 + 0.3);
            } else if (cycle_for_months < 9 && cycle_for_months >= 6) {
                LT = precisionRound(LT + 0.2 + 0.3);
            } else if (cycle_for_months < 12 && cycle_for_months >= 9) {
                LT = precisionRound(LT + 0.3 + 0.3);
            } else if (cycle_for_months < 18 && cycle_for_months >= 12) {
                LT = precisionRound(LT + 0.4 + 0.3);
            } else {
                LT = precisionRound(LT + 0.5 + 0.3);
            }
        } else if (
            contract.principal >= 1000000000 &&
            contract.principal < 5000000000
        ) {
            if (cycle_for_months < 3) {
                LT = precisionRound(LT + 0.4);
            } else if (cycle_for_months < 6 && cycle_for_months >= 3) {
                LT = precisionRound(LT + 0.1 + 0.4);
            } else if (cycle_for_months < 9 && cycle_for_months >= 6) {
                LT = precisionRound(LT + 0.2 + 0.4);
            } else if (cycle_for_months < 12 && cycle_for_months >= 9) {
                LT = precisionRound(LT + 0.3 + 0.4);
            } else if (cycle_for_months < 18 && cycle_for_months >= 12) {
                LT = precisionRound(LT + 0.4 + 0.4);
            } else {
                LT = precisionRound(LT + 0.5 + 0.4);
            }
        } else {
            if (cycle_for_months < 3) {
                LT = precisionRound(LT + 0.5);
            } else if (cycle_for_months < 6 && cycle_for_months >= 3) {
                LT = precisionRound(LT + 0.1 + 0.5);
            } else if (cycle_for_months < 9 && cycle_for_months >= 6) {
                LT = precisionRound(LT + 0.2 + 0.5);
            } else if (cycle_for_months < 12 && cycle_for_months >= 9) {
                LT = precisionRound(LT + 0.3 + 0.5);
            } else if (cycle_for_months < 18 && cycle_for_months >= 12) {
                LT = precisionRound(LT + 0.4 + 0.5);
            } else {
                LT = precisionRound(LT + 0.5 + 0.5);
            }
        }
        const period_interest = precisionRound(
            (Math.pow(1 + precisionRound(LT / 100), cycle_for_months) - 1) * 100
        ).toFixed(2); // lãi kỳ
        const disbursement = precisionRound(
            contract.principal *
                precisionRound(parseFloat(period_interest) / 100 + 1)
        );
        return disbursement;
    }
    async getDisbursementAgriculture(contract: any) {
        const seasons = parseInt(contract.cycle);
        if (seasons < 2) {
            throw Error(`Bởi vì số mùa gửi nhỏ hơn 2. Nên không có giải ngân`);
        }
        const disbursement = precisionRound(
            contract.principal *
                seasons *
                precisionRound((0.15 * seasons) / 2 + 1)
        );
        return disbursement;
    }
    async getDisbursement(idContract: any) {
        const contract: any = await this.getById(idContract);
        if (!contract) throw new BadRequestError(`Hợp đồng không tồn tại`);
        let disbursement;
        if (contract.type == CONTRACT_ENUM_STATUS_TYPE.USD) {
            disbursement = await this.getDisbursementUSD(contract);
        } else {
            disbursement = await this.getDisbursementAgriculture(
                contract
            );

        }
        return disbursement;
    }

    async handleContract(idContract: any, status = 'Completed') {
        const contract: any = await this.getById(idContract);
        if (!contract) throw new BadRequestError(`Hợp đồng không tồn tại`);
        const user: any = await userServices.getById(contract.userId);
        if (status === CONTRACT_ENUM_STATUS.Completed) {
            if (contract.status !== CONTRACT_ENUM_STATUS.Confirmed) {
                throw new BadRequestError(
                    `Hợp đồng muốn được trạng thái ${CONTRACT_ENUM_STATUS.Completed} thì phải ở trạng thái ${CONTRACT_ENUM_STATUS.Confirmed}`
                );
            }
            const balanceNow = user.Wallet.balance;
            const balanceNew = precisionRound(
                balanceNow + contract.principal + contract.interest_rate
            );
            await userServices.Update(contract.userId, {
                'Wallet.balance': balanceNew
            });
            sendMail(
                JSON.stringify({
                    type: 'mailCompleteContract',
                    data: {
                        user,
                        contract
                    }
                })
            );
            return await this.Update(idContract, {
                status: CONTRACT_ENUM_STATUS.Completed
            });
        } else if (status === CONTRACT_ENUM_STATUS.Canceled) {
            if (contract.status !== CONTRACT_ENUM_STATUS.Confirmed) {
                const balanceNow = user.Wallet.balance;
                const balanceNew = precisionRound(
                    balanceNow + contract.principal
                );
                await userServices.Update(contract.userId, {
                    'Wallet.balance': balanceNew
                });
                return await this.Update(idContract, {
                    status: CONTRACT_ENUM_STATUS.Canceled
                });
            } else if (contract.status !== CONTRACT_ENUM_STATUS.Completed) {
                const balanceNow = user.Wallet.balance;
                const balanceNew = precisionRound(
                    balanceNow - contract.interest_rate
                );
                if (balanceNew < 0)
                    throw new BadRequestError(
                        `Số dư tài khoản không đủ để thực hiện ${CONTRACT_ENUM_STATUS.Canceled} hợp đồng`
                    );
                await userServices.Update(contract.userId, {
                    'Wallet.balance': balanceNew
                });
                return await this.Update(idContract, {
                    status: CONTRACT_ENUM_STATUS.Canceled
                });
            } else {
                throw new BadRequestError(
                    `Hợp đồng muốn được trạng thái ${CONTRACT_ENUM_STATUS.Canceled} thì phải ở trạng thái ${CONTRACT_ENUM_STATUS.Confirmed}, ${CONTRACT_ENUM_STATUS.Completed}`
                );
            }
        } else if (status === CONTRACT_ENUM_STATUS.Confirmed) {
            if (contract.status !== CONTRACT_ENUM_STATUS.Pending) {
                throw new BadRequestError(
                    `Hợp đồng muốn được trạng thái ${CONTRACT_ENUM_STATUS.Confirmed} thì phải ở trạng thái ${CONTRACT_ENUM_STATUS.Pending}`
                );
            }
            const day = parseInt(contract.number_of_days_taken);
            const principal = parseInt(contract.principal);
            const rate = parseFloat(contract.rate);
            const cycle = parseInt(contract.cycle) * 30;
            const balanceNow = user.Wallet.balance;
            const balanceNew = precisionRound(balanceNow - contract.principal);
            const day_new = day + 1;
            const interest_rate = precisionRound(
                principal * precisionRound((rate / cycle) * day_new)
            );
            if (balanceNew < 0)
                throw new BadRequestError(
                    `Số dư tài khoản không đủ để thực hiện hợp đồng`
                );
            await userServices.Update(contract.userId, {
                'Wallet.balance': balanceNew
            });
            sendMail(
                JSON.stringify({
                    type: 'mailConfirmContract',
                    data: {
                        user,
                        contract
                    }
                })
            );
            return await this.Update(idContract, {
                status: CONTRACT_ENUM_STATUS.Confirmed,
                number_of_days_taken: day_new,
                interest_rate: interest_rate
            });
        } else if (status === CONTRACT_ENUM_STATUS.Pending) {
            if (contract.status !== CONTRACT_ENUM_STATUS.Canceled) {
                throw new BadRequestError(
                    `Hợp đồng muốn được trạng thái ${CONTRACT_ENUM_STATUS.Pending} thì phải ở trạng thái ${CONTRACT_ENUM_STATUS.Canceled}`
                );
            }
            return await this.Update(idContract, {
                status: CONTRACT_ENUM_STATUS.Pending
            });
        } else {
            throw new BadRequestError(
                `${status} không được hỗ trợ cho hợp đồng`
            );
        }
    }
})();
