import Rate from '../models/rate.model';
import BaseServices from './base.services';

export default new (class RateServices implements BaseServices {
    async Create(data: any) {
        return await Rate.create(data);
    }
    async Update(id: any, data: any) {
        await Rate.update(data, { where: { id: id } });
        return 'Sửa tỉ lệ thành công.';
    }
    async Delete(id: any) {
        await Rate.destroy({ where: { id: id } });
        return 'Xoá tỉ lệ thành công.';
    }
    async getById(id: any) {
        return await Rate.findByPk(id);
    }
    async getAll() {
        return await Rate.findAll();
    }
})();
