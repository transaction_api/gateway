/* eslint-disable @typescript-eslint/no-explicit-any */
import { ConflictRequestError } from '../core/error.response';
import Payment from '../models/payment.model';
import BaseServices from './base.services';

export default new (class PaymentServices implements BaseServices {
    async Create(data: any) {
        try {
            return await Payment.create(data);
        } catch (error: any) {
            throw new ConflictRequestError(error);
        }
    }
    async Update(id: any, data: any) {
        await Payment.update(data, { where: { id: id } });
        return 'Update payment successfully with idPayment = ' + id;
    }
    async Delete(id: any) {
        await Payment.destroy({ where: { id: id } });
        return 'Delete payment successfully with idPayment = ' + id;
    }
    async getById(id: any) {
        return await Payment.findByPk(id);
    }
    async getAll() {
        return await Payment.findAll();
    }
    async getByAccountNumber(
        number: number,
        bankName: string,
        accountName: string
    ) {
        return await Payment.findOne({
            where: {
                account_number: number,
                bank_name: bankName,
                account_name: accountName
            }
        });
    }
})();
