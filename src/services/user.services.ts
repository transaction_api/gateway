import otpGenerator from 'otp-generator';
/* eslint-disable @typescript-eslint/no-explicit-any */
import { BadRequestError } from '../core/error.response';
import { userModel } from '../models/user.model';
import BaseServices from './base.services';
import bcrypt from 'bcrypt';
import { saltBcrypt } from '../utils/functions.utils';
import sendMail from '../queues/sendMail';
import { USER_TYPE } from '../types/user.types';

export default new (class UserServices implements BaseServices {
    async Create(data: any) {
        return await userModel.create(data);
    }
    async Update(id: any, data: any) {
        await userModel.findByIdAndUpdate(id, { $set: data }).lean();
        return 'Update user successfully';
    }
    async Delete(id: any) {
        await userModel.findByIdAndRemove(id).lean();
        return 'Delete user successfully';
    }
    async getById(id: any) {
        return await userModel.findById(id).lean();
    }
    async getAll() {
        return await userModel.find().lean();
    }
    async getByUsername(username: string) {
        return await userModel.findOne({ 'payment.username': username }).lean();
    }
    async getByEmail(email: string) {
        return await userModel.findOne({ 'payment.email': email }).lean();
    }
    async changeRole(id: any, role: string) {
        await userModel.findByIdAndUpdate(id, { 'payment.roles': role }).lean();
        return 'Thay đổi role của user thành công sang ' + role;
    }
    async checkPassword(hashed: string, password: string) {
        const checked = await bcrypt.compareSync(password, hashed);
        if (!checked)
            throw new BadRequestError('Password or Username không đúng');
        return 'OK';
    }
    forgotPassword = async (email: string) => {
        const user: any = await this.getByEmail(email);
        if (!user) throw new BadRequestError(`Người dùng không tồn tại`);
        const newPass = otpGenerator.generate(10);
        const hashed = bcrypt.hashSync(newPass, saltBcrypt);
        await this.Update(user._id, {
            'payment.password': hashed
        });
        sendMail(
            JSON.stringify({
                type: 'forgotPassword',
                data: {
                    user,
                    newPass
                }
            })
        );
        return 'Đã thay đổi password thành công vui lòng vào email để dùng mật khẩu mới.';
    };

    paging = async (page = 1, show = 10) => {
        const start = (page - 1) * show;
        const end = page * show;
        const users: Array<any> = await userModel
            .find()
            .sort({ createdAt: 'desc' })
            .lean();
        const result = users.slice(start, end);
        return result;
    };

    pagingSearch = async (page = 1, show = 10, search = '') => {
        if (!search) {
            const users: Array<any> = await this.paging(page, show);
            return users;
        } else {
            const users = await this.getAll();
            const filtered: Array<any> = users.filter(
                (user: any) =>
                    user.payment.email.toLowerCase().includes(search) ||
                    user.payment.username.toLowerCase().includes(search) ||
                    user.rank.toLowerCase().includes(search)
            );
            const start = (page - 1) * show;
            const end = page * show;
            return filtered.slice(start, end);
        }
    };

    userHadBalance = async () => {
        const users = await this.getAll();
        const userHad = users.filter((user: any) => user.Wallet.balance > 0);
        const totalBalance = userHad.reduce(
            (total: number, user: any) => total + user.Wallet.balance,
            0
        );

        return {
            totalBalance,
            userHad
        };
    };
})();
