/* eslint-disable @typescript-eslint/no-explicit-any */
import { BadRequestError } from '../core/error.response';
import { WITHDRAW_ENUM_STATUS } from '../enum/withdraw.enum';
import Payment from '../models/payment.model';
import Withdraw from '../models/withdraw.model';
import sendMail from '../queues/sendMail';
import { precisionRound } from '../utils/functions.utils';
import BaseServices from './base.services';
import userServices from './user.services';
import Deposit from '../models/deposit.model';
import { DEPOSIT_ENUM_STATUS } from '../enum/deposit.enum';

export default new (class WithdrawServices implements BaseServices {
    async Create(data: any) {
        const payment: any = await Payment.findByPk(data.idPayment);
        return await payment.createWithdraw(data);
    }
    async Update(id: any, data: any) {
        await Withdraw.update(data, { where: { id: id } });
        return 'Update withdraw successfully with idWithdraw = ' + id;
    }
    async Delete(id: any) {
        await Withdraw.destroy({ where: { id: id } });
        return 'Delete withdraw successfully with idWithdraw = ' + id;
    }
    async getById(id: any) {
        return await Withdraw.findByPk(id);
    }
    async getAll() {
        return await Withdraw.findAll();
    }
    async getWithdrawAndPayment(idWithdraw: any) {
        const withdraw = await Withdraw.findOne({
            where: {
                id: idWithdraw
            },
            include: [Payment]
        });
        return withdraw;
    }

    paging = async (page = 1, show = 10) => {
        const start = (page - 1) * show;
        const end = page * show;

        const withdraws: Array<any> = await Withdraw
            .findAll({
                include: [Payment]
            })
        return withdraws.slice(start, end);;
    };

    pagingSearch = async (page = 1, show = 10, search = '') => {
        if (!search) {
            const withdraws = await this.paging(page, show);
            return withdraws;
        } else {
            const withdraws = await this.getAll();
            const filtered = withdraws.filter(
                (deposit: any) =>
                    deposit.userId.toLowerCase().includes(search) ||
                    deposit.status.toLowerCase().includes(search)
            );
            const start = (page - 1) * show;
            const end = page * show;
            return filtered.slice(start, end);
        }
    };

    totalWithdraw = async () => {
        const withdraws = await this.getAll();
        let total;
        if (withdraws.length === 0) {
            total = 0;
        } else {
            total = withdraws
                .filter(
                    (withdraw: any) =>
                        withdraw.status === WITHDRAW_ENUM_STATUS.Completed
                )
                .reduce(
                    (total: number, withdraw: any) => total + withdraw.amount,
                    0
                );
        }
        return total;
    };

    handleWithdraw = async (idWithdraw: any, status = 'Completed') => {
        const withdraw: any = await this.getById(idWithdraw);
        const user: any = await userServices.getById(withdraw.userId);
        if (!withdraw) throw new BadRequestError(`Withdraw is not exist`);
        if (!user) throw new BadRequestError(`User is not exist`);

        if (status === WITHDRAW_ENUM_STATUS.Completed) {
            if (withdraw.status !== WITHDRAW_ENUM_STATUS.Confirmed) {
                throw new BadRequestError(
                    `Please ${WITHDRAW_ENUM_STATUS.Confirmed} this withdraw order`
                );
            }
            await this.Update(idWithdraw, {
                status: WITHDRAW_ENUM_STATUS.Completed
            });
            sendMail(
                JSON.stringify({
                    type: 'mailWithdraw',
                    data: {
                        user,
                        amount: withdraw.amount
                    }
                })
            );
            return `${WITHDRAW_ENUM_STATUS.Completed} this order withdraw`;
        } else if (status === WITHDRAW_ENUM_STATUS.Canceled) {
            if (withdraw.status === WITHDRAW_ENUM_STATUS.Completed) {
                const newBalance = precisionRound(
                    user.Wallet.balance + withdraw.amount
                );
                const newWithdraw = precisionRound(
                    user.Wallet.withdraw - withdraw.amount
                );
                await userServices.Update(withdraw.userId, {
                    'Wallet.balance': newBalance,
                    'Wallet.withdraw': newWithdraw
                });
                await this.Update(idWithdraw, {
                    status: WITHDRAW_ENUM_STATUS.Canceled
                });
                return `${WITHDRAW_ENUM_STATUS.Canceled} this order withdraw`;
            } else if (withdraw.status === WITHDRAW_ENUM_STATUS.Confirmed) {
                const newBalance = precisionRound(
                    user.Wallet.balance + withdraw.amount
                );
                const newWithdraw = precisionRound(
                    user.Wallet.withdraw - withdraw.amount
                );
                await userServices.Update(withdraw.userId, {
                    'Wallet.balance': newBalance,
                    'Wallet.withdraw': newWithdraw
                });
                await this.Update(idWithdraw, {
                    status: WITHDRAW_ENUM_STATUS.Canceled
                });
                return `${WITHDRAW_ENUM_STATUS.Canceled} this order withdraw`;
            } else {
                await this.Update(idWithdraw, {
                    status: WITHDRAW_ENUM_STATUS.Canceled
                });
                return `${WITHDRAW_ENUM_STATUS.Canceled} this order withdraw`;
            }
        } else if (status === WITHDRAW_ENUM_STATUS.Confirmed) {
            if (withdraw.status !== WITHDRAW_ENUM_STATUS.Pending) {
                throw new BadRequestError(
                    `Please ${WITHDRAW_ENUM_STATUS.Pending} this withdraw order`
                );
            }
            const newBalance = precisionRound(
                user.Wallet.balance - withdraw.amount
            );
            const newWithdraw = precisionRound(
                user.Wallet.withdraw + withdraw.amount
            );
            if (newBalance < 0)
                throw new BadRequestError(
                    `Your balance is not enough for order withdraw`
                );
            await userServices.Update(withdraw.userId, {
                'Wallet.balance': newBalance,
                'Wallet.withdraw': newWithdraw
            });
            await this.Update(idWithdraw, {
                status: WITHDRAW_ENUM_STATUS.Confirmed
            });
            return `${WITHDRAW_ENUM_STATUS.Confirmed} this order withdraw`;
        } else if (status === WITHDRAW_ENUM_STATUS.Pending) {
            if (withdraw.status !== WITHDRAW_ENUM_STATUS.Canceled) {
                throw new BadRequestError(
                    `Please ${WITHDRAW_ENUM_STATUS.Canceled} this withdraw order`
                );
            }
            await this.Update(idWithdraw, {
                status: WITHDRAW_ENUM_STATUS.Pending
            });
            return `${WITHDRAW_ENUM_STATUS.Pending} this order withdraw`;
        } else {
            throw new BadRequestError(`${status}} is not supported`);
        }
    };
})();
