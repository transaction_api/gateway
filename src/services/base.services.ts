export default interface BaseServices {
    Create(data: any): any;
    Update(id: any, data: any): any;
    Delete(id: any): any;
    getById(id: any): any;
    getAll(): any;
}
