/* eslint-disable @typescript-eslint/no-explicit-any */
import { BadRequestError } from '../core/error.response';
import { DEPOSIT_ENUM_STATUS } from '../enum/deposit.enum';
import Deposit from '../models/deposit.model';
import Payment from '../models/payment.model';
import sendMail from '../queues/sendMail';
import { precisionRound } from '../utils/functions.utils';
import BaseServices from './base.services';
import userServices from './user.services';
import { DEPOSIT_TYPE } from '../types/deposit.types';

export default new (class DepositServices implements BaseServices {
    async Create(data: any) {
        // return await Deposit.create(data);
        const payment: any = await Payment.findByPk(data.idPayment);
        return await payment.createDeposit(data);
    }
    async Update(id: any, data: any) {
        await Deposit.update(data, { where: { id: id } });
        return 'Sửa đổi yêu cầu nạp tiền thành công idDeposit = ' + id;
    }
    async Delete(id: any) {
        await Deposit.destroy({ where: { id: id } });
        return 'Xoá yêu cầu nạp tiền thành công idDeposit = ' + id;
    }
    async getById(id: any) {
        return await Deposit.findByPk(id);
    }
    async getAll() {
        return await Deposit.findAll();
    }
    async getDepositAndPayment(idDeposit: any) {
        const deposit = await Deposit.findOne({
            where: {
                id: idDeposit
            },
            include: [Payment]
        });
        return deposit;
    }

    paging = async (page = 1, show = 10) => {
        const start = (page - 1) * show;
        const end = page * show;

        const deposits: Array<any> = await Deposit.findAll({
            include: [Payment]
        });
        return deposits.slice(start, end);
    };

    pagingSearch = async (page = 1, show = 10, search = '') => {
        if (!search) {
            const deposits = await this.paging(page, show);
            return deposits;
        } else {
            const deposits = await this.getAll();
            const filtered = deposits.filter(
                (deposit: any) =>
                    deposit.userId.toLowerCase().includes(search) ||
                    deposit.status.toLowerCase().includes(search)
            );
            const start = (page - 1) * show;
            const end = page * show;
            return filtered.slice(start, end);
        }
    };

    totalDeposit = async () => {
        const deposits = await this.getAll();
        let total;
        if (deposits.length === 0) {
            total = 0;
        } else {
            total = deposits
                .filter(
                    (deposit: any) =>
                        deposit.status === DEPOSIT_ENUM_STATUS.Completed
                )
                .reduce(
                    (total: number, deposit: any) => total + deposit.amount,
                    0
                );
        }
        return total;
    };

    handleDeposit = async (
        idDeposit: any,
        status = DEPOSIT_ENUM_STATUS.Completed
    ) => {
        const deposit: any = await this.getById(idDeposit);
        if (!deposit)
            throw new BadRequestError(`Yêu cầu nạp tiền không tồn tại`);
        const user: any = await userServices.getById(deposit.userId);
        if (!user) throw new BadRequestError(`Người dùng không tồn tại`);

        if (status === DEPOSIT_ENUM_STATUS.Completed) {
            if (deposit.status !== DEPOSIT_ENUM_STATUS.Pending) {
                throw new BadRequestError();
            }

            if (deposit.statement === '') {
                throw new BadRequestError(
                    `Người dùng vẫn chưa gửi hình ảnh chuyển khoản nên không thể hoàn thành giao dịch.`
                );
            }

            await userServices.Update(deposit.userId, {
                'Wallet.balance': precisionRound(
                    user.Wallet.balance + deposit.amount
                ),
                'Wallet.deposit': precisionRound(
                    user.Wallet.deposit + deposit.amount
                )
            });
            sendMail(
                JSON.stringify({
                    type: 'mailDeposit',
                    data: {
                        user,
                        amount: deposit.amount
                    }
                })
            );
            return await this.Update(deposit.id, {
                status: DEPOSIT_ENUM_STATUS.Completed
            });
        } else if (status === DEPOSIT_ENUM_STATUS.Canceled) {
            if (deposit.status === DEPOSIT_ENUM_STATUS.Completed) {
                const newBalance = precisionRound(
                    user.Wallet.balance - deposit.amount
                );
                if (newBalance < 0)
                    throw new BadRequestError(
                        'Số dư của người dùng không đủ để thực hiện giao dịch.'
                    );

                await userServices.Update(deposit.userId, {
                    'Wallet.balance': newBalance,
                    'Wallet.deposit': precisionRound(
                        user.Wallet.deposit - deposit.amount
                    )
                });
                return await this.Update(deposit.id, {
                    status: DEPOSIT_ENUM_STATUS.Canceled
                });
            } else {
                return await this.Update(deposit.id, {
                    status: DEPOSIT_ENUM_STATUS.Canceled
                });
            }
        } else if (status === DEPOSIT_ENUM_STATUS.Pending) {
            if (deposit.status !== DEPOSIT_ENUM_STATUS.Canceled) {
                throw new BadRequestError(
                    `Nếu muốn đổi sang trạng thái ${DEPOSIT_ENUM_STATUS.Pending} thì giao dịch phải ở trạng thái ${DEPOSIT_ENUM_STATUS.Canceled}`
                );
            }
            return await this.Update(deposit.id, {
                status: DEPOSIT_ENUM_STATUS.Pending
            });
        } else {
            throw new BadRequestError(
                `${status} không được hỗ trợ trong dịch vụ này`
            );
        }
    };
})();
