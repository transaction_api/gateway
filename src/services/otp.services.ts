/* eslint-disable @typescript-eslint/no-explicit-any */
import { otpModel } from '../models/otp.model';
import BaseServices from './base.services';

export default new (class OTPServices implements BaseServices {
    async Create(data: any) {
        return await otpModel.create(data);
    }
    async Update(id: any, data: any) {
        await otpModel.findByIdAndUpdate(id, { $set: data }).lean();
        return 'Update OTP successfully with idOTP = ' + id;
    }
    async Delete(id: any) {
        await otpModel.findByIdAndDelete(id).lean();
        return 'Delete OTP successfully with idOTP = ' + id;
    }
    async getById(id: any) {
        return otpModel.findById(id).lean();
    }
    async getAll() {
        return await otpModel.find().lean();
    }
    async getByOTP(otp: string) {
        return await otpModel.findOne({ code: otp }).lean();
    }
    async getByIdUser(idUser: any) {
        return await otpModel.find({ idUser: idUser }).lean();
    }
})();
