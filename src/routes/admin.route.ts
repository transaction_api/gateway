import express from 'express';
import adminController from '../controllers/admin.controller';
import multer from 'multer';
import { verifyPermission, verifyToken } from '../middleWares/checkToken';
const adminRouter = express.Router();

const upload = multer({ dest: 'uploads/images' });

// ---------------------------------- [GET] ----------------------------------

// [GET] /admin/deposit/:idDeposit
adminRouter.get('/deposit/:idDeposit', adminController.getDeposit);

// [GET] /admin/deposit
adminRouter.get('/deposit', adminController.getAllDeposits);

// [GET] /admin/withdraw/:idWithdraw
adminRouter.get('/withdraw/:idWithdraw', adminController.getWithdraw);

// [GET] /admin/withdraw
adminRouter.get('/withdraw', adminController.getAllWithdraws);

// [GET] /admin/user/:idUser
adminRouter.get('/user/:idUser', adminController.getUser);

// [GET] /admin/user
adminRouter.get('/user', adminController.getAllUsers);

// [GET] /admin/contract/:idContract
adminRouter.get('/contract/:idContract', adminController.getContract);

// [GET] /admin/contract
adminRouter.get('/contract', adminController.getAllContracts);

// [GET] /admin/rate
adminRouter.get('/rate', adminController.getRate);

// [GET] /admin/payment
adminRouter.get('/payment', adminController.getAllPayments);

// [GET] /admin/payment/:idPayment
adminRouter.get('/payment/:idPayment', adminController.getPaymentById);

// [GET] /admin/payments
adminRouter.get('/payments', adminController.getAllPaymentByType);

// [GET] /total
adminRouter.get('/total', adminController.totalDashboard);

// ---------------------------------- [GET] ----------------------------------

// --------------------------------------------------[PAGING]--------------------------------------------------

// [GET] /admin/paging/user
adminRouter.get('/paging/user', adminController.pagingUser);

// [GET] /admin/paging/deposit
adminRouter.get('/paging/deposit', adminController.pagingDeposit);

// [GET] /admin/paging/withdraw
adminRouter.get('/paging/withdraw', adminController.pagingWithdraw);

// --------------------------------------------------[PAGING]--------------------------------------------------

// ---------------------------------- [HANDLE] ----------------------------------

adminRouter.use(verifyToken);
adminRouter.use(verifyPermission(['admin', 'manager']));

// [PUT] /admin/handle/contract/:idContract
adminRouter.put('/handle/contract/:idContract', adminController.handleContract);

// [PUT] /admin/handle/deposit/:idDeposit
adminRouter.put('/handle/deposit/:idDeposit', adminController.handleDeposit);

// [PUT] /admin/handle/withdraw/:idWithdraw
adminRouter.put('/handle/withdraw/:idWithdraw', adminController.handleWithdraw);

// ---------------------------------- [HANDLE] ----------------------------------

// ---------------------------------- [PUT, POST, DELETE] ----------------------------------

// [PUT] /admin/deposit/:idDeposit
adminRouter.put('/deposit/:idDeposit', adminController.updateDeposit);

// [DELETE] /admin/deposit/:idDeposit
adminRouter.delete('/deposit/:idDeposit', adminController.deleteDeposit);

// [PUT] /admin/withdraw/:idWithdraw
adminRouter.put('/withdraw/:idWithdraw', adminController.updateWithdraw);

// [DELETE] /admin/withdraw/:idWithdraw
adminRouter.delete('/withdraw/:idWithdraw', adminController.deleteWithdraw);

// [PUT] /admin/user/:idUser
adminRouter.put('/user/:idUser', adminController.updateUser);

// [DELETE] /admin/user/:idUser
adminRouter.delete('/user/:idUser', adminController.deleteUser);

// [PUT] /admin/contract/image/:idContract
adminRouter.put(
    '/contract/image/:idContract',
    upload.single('statement'),
    adminController.addImageContract
);

// [PUT] /admin/contract/:idContract
adminRouter.put('/contract/:idContract', adminController.updateContract);

// [DELETE] /admin/contract/:idContract
adminRouter.delete('/contract/:idContract', adminController.deleteContract);

// [POST] /admin/rate
adminRouter.post('/rate', adminController.createRate);

// [PUT] /admin/rate/:idRate
adminRouter.put('/rate/:idRate', adminController.updateRate);

// [DELETE] /admin/rate/:idRate
adminRouter.delete('/rate/:idRate', adminController.deleteRate);

// [POST] /admin/payment
adminRouter.post('/payment', adminController.createPayment);

// [PUT] /admin/payment/:idPayment
adminRouter.put('/payment/:idPayment', adminController.updatePayment);

// [DELETE] /admin/payment/:idPayment
adminRouter.delete('/payment/:idPayment', adminController.deletePayment);

// ---------------------------------- [PUT, POST, DELETE] ----------------------------------

// ---------------------------------- [ADMIN] ----------------------------------

adminRouter.use(verifyPermission(['admin']));

// [PUT] /admin/user/role/:idUser
adminRouter.put('/user/role/:idUser', adminController.changeRole);

// ---------------------------------- [ADMIN] ----------------------------------

export default adminRouter;
