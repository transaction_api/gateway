import express from 'express';
import userController from '../controllers/user.controller';
import multer from 'multer';
import { verifyToken } from '../middleWares/checkToken';
import path from 'path';

const userRouter = express.Router();
const upload = multer({ dest: 'uploads/images' });

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/images_user');
    },

    filename: function (req, file, cb) {
        cb(
            null,
            file.fieldname + '-' + Date.now() + path.extname(file.originalname)
        );
    }
});

const uploadMulti = multer({ storage: storage });

const cpUpload = uploadMulti.fields([
    { name: 'cccdFont', maxCount: 1 },
    { name: 'cccdBeside', maxCount: 1 },
    { name: 'licenseFont', maxCount: 1 },
    { name: 'licenseBeside', maxCount: 1 }
]);

// ---------------------------------- [GET] ----------------------------------

// [GET] /users/withdraw/otp/resend/:idUser/:idWithdraw
userRouter.get(
    '/withdraw/otp/resend/:idUser/:idWithdraw',
    userController.resendOtpWithdraw
);

// [GET] /users/contract/destroy/:idContract
userRouter.get('/contract/destroy/:idContract', userController.destroyContract);

// [GET] /users/contract/disbursement/:idContract
userRouter.get(
    '/contract/disbursement/:idContract',
    userController.getDisbursement
);

// [GET] /users/contract/:idUser
userRouter.get('/contract/:idUser', userController.getAllContract);

// [GET] /user/forgot/password/:email
userRouter.get('/forgot/password/:email', userController.forgotPassword);

// [GET] /users/dashboard/:idUser
userRouter.get('/dashboard/:idUser', userController.DashBoard);

// ---------------------------------- [GET] ----------------------------------

// ---------------------------------- [PUT, POST, DELETE] ----------------------------------

userRouter.use(verifyToken);

// [POST] /users/contract/disbursement/filed
userRouter.post(
    '/contract/disbursement/filed',
    userController.getDisbursementByFiled
);

userRouter.put('/payment/:idUser', userController.addPayment);

// [POST] /users/deposit/:idUser
userRouter.post('/deposit/:idUser', userController.deposit);

// [PUT] /users/deposit/image/:idDeposit
userRouter.put(
    '/deposit/image/:idDeposit',
    upload.single('statement'),
    userController.addImageDeposit
);

// [POST] /users/withdraw/:idUser
userRouter.post('/withdraw/:idUser', userController.withdraw);

// [PUT] /users/withdraw/otp
userRouter.put('/withdraw/otp', userController.otpWithdraw);

// [POST] /users/withdraw/cancel/:idWithdraw
userRouter.post('/withdraw/cancel/:idWithdraw', userController.cancelWithdraw);

// [POST] /users/contract/:idUser
userRouter.post('/contract/:idUser', userController.contract);

// [PUT] /user/image/:idUser
userRouter.put('/image/:idUser', cpUpload, userController.updateImageUser);

// ---------------------------------- [PUT, POST, DELETE] ----------------------------------

export default userRouter;
