import express from 'express';
import authController from '../controllers/auth.controller';
const authRouter = express.Router();

authRouter.post('/register', authController.register);

// [POST] /auth/register/admin
authRouter.post('/register/admin', authController.registerAdmin);

authRouter.post('/login', authController.login);

authRouter.post('/refreshToken/:email', authController.refreshToken);

authRouter.post('/logout/:email', authController.logout);

export default authRouter;
