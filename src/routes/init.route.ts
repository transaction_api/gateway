/* eslint-disable @typescript-eslint/no-explicit-any */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { Express, Request, Response, NextFunction } from 'express';
import { LoggerErr } from '../utils/logger';
import authRouter from './auth.route';
import userRouter from './user.route';
import adminRouter from './admin.route';

const routes = (app: Express) => {
    app.use('/auth', authRouter);
    app.use('/users', userRouter);
    app.use('/admin', adminRouter);

    app.use((req: Request, res: Response, next: NextFunction) => {
        res.status(200).json({
            code: 200,
            message: "welcome to api transaction"
        })
    })

    app.use((err: any, req: Request, res: Response, next: NextFunction) => {
        const LoggerMessage: any = new LoggerErr(err.name);
        const Logger_: any = LoggerMessage.createLogger();
        Logger_.error(err.message.toUpperCase());
        const statsCode = err.status || 500;
        res.status(statsCode).json({
            code: statsCode,
            message: err.message || 'Internal server error'
        });
    });
};

export default routes;
