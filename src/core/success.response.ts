import { Response } from 'express';
const StatusCode = {
    CREATED: 201,
    OK: 200
};

const ReasonStatusCode = {
    CREATED: 'CREATED',
    OK: 'SUCCESS'
};

class SuccessResponse {
    message: string;
    status: number;
    metadata: any;

    constructor({
        message = '',
        statusCode = StatusCode.OK,
        reasonStatusCode = ReasonStatusCode.OK,
        metadata = {}
    }) {
        this.message = !message ? reasonStatusCode : message;
        this.status = statusCode;
        this.metadata = metadata;
    }

    send(res: Response, headers = {}) {
        return res.status(this.status).json(this);
    }
}

class Ok extends SuccessResponse {
    constructor({ message = '', metadata = {} }) {
        super({ message, metadata });
    }
}

class CREATED extends SuccessResponse {
    constructor({
        message = '',
        metadata = {},
        statusCode = StatusCode.CREATED,
        reasonStatusCode = ReasonStatusCode.CREATED
    }) {
        super({ message, statusCode, reasonStatusCode, metadata });
    }
}

export { Ok, CREATED };
