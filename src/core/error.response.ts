const StatusCode = {
    FORBIDDEN: 403,
    CONFLICT: 409
};

const ReasonStatusCode = {
    FORBIDDEN: 'Bad request Error',
    CONFLICT: 'Conflict error'
};

class ErrorResponse extends Error {
    public status: number;

    constructor(message: any, status: any) {
        super(message);
        this.status = status;
    }
}

class ConflictRequestError extends ErrorResponse {
    constructor(
        message = ReasonStatusCode.CONFLICT,
        status = StatusCode.CONFLICT
    ) {
        super(message, status);
    }
}

class BadRequestError extends ErrorResponse {
    constructor(
        message = ReasonStatusCode.FORBIDDEN,
        status = StatusCode.FORBIDDEN
    ) {
        super(message, status);
    }
}

export { ConflictRequestError, BadRequestError };
