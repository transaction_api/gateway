export type USER_TYPE = {
    id: any;
    Wallet: {
        balance: number;
        deposit: number;
        withdraw: number;
    };
    payment: {
        bank: number;
        roles: string;
        email: string;
        password: string;
        username: string;
    };
    contract: any;
    rank: string;
    changeBalance: number;
    uploadCCCDFont: string;
    uploadCCCDBeside: string;
    uploadLicenseFont: string;
    uploadLicenseBeside: string;
    lock: boolean;
    activated: boolean;
    createdAt: Date;
    updatedAt: Date;
};
