export type PAYMENT_TYPE = {
    id: any;
    type_payment: any;
    bank_name: string;
    account_name: string;
    account_number: string;
};
