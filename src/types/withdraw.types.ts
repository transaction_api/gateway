export type WITHDRAW_TYPE = {
    id: any;
    userId: any;
    status: string;
    note: string;
    amount: number;
};
