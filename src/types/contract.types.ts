export type CONTRACT_TYPE = {
    userId: any;
    status: any;
    rate: number;
    principal: number; /// tiền gốc
    interest_rate: number; /// tiền lãi
    cycle: number; /// chu kì gửi
    number_of_days_taken: number; /// số ngày đã gửi
    type: string; /// loại gửi
    statement: string; //// hình ảnh về hợp đồng
    date_start: Date;
};
