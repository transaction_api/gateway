export type OTP_TYPE = {
    idUser: any;
    code: string;
    type: string;
    idServices: string;
};
