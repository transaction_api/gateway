export type DEPOSIT_TYPE = {
    id: any;
    userId: any;
    status: string;
    statement: string;
    note: string;
    amount: number;
    idPayment: number;
};
