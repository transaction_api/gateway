/* eslint-disable @typescript-eslint/no-explicit-any */
import jwt from 'jsonwebtoken';
import { NextFunction, Request, Response } from 'express';
import redisClient from '../databases/init.redis';
import { BadRequestError } from '../core/error.response';
import userServices from '../services/user.services';
import { USER_TYPE } from '../types/user.types';

const verifyToken = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const token: string =
            req.body?.token ||
            req?.headers['token'] ||
            req.body?.headers?.token;
        // console.log(req.headers.authorization?.split(' ')[1]);
        // const token: any = req.headers['token'];
        if (token == null || token === undefined) {
            throw new BadRequestError('A token is required for authentication');
        }
        try {
            const decoded: any = await jwt.verify(
                token,
                `${process.env.ACCESS_TOKEN_JWT_SECRET}`
            );
            const userFindById = await userServices.getById(decoded.uid);
            if (!userFindById) {
                throw new BadRequestError('User is exist ?');
            }
            const findAccessToken = await redisClient.get(
                `${decoded.uid}_access_token`
            );
            if (token !== findAccessToken || !findAccessToken) {
                throw new BadRequestError('Token is not true or is expired');
            } else {
                res.locals.user = userFindById;
                next();
            }
        } catch (error) {
            next(error);
        }
    } catch (error: any) {
        next(error);
    }
};

const check_lock = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const user: any = res.locals.user;
        if (user.lock) {
            throw new BadRequestError(
                `User is locked. Please contact to admin to unlock.`
            );
        }
        next();
    } catch (error: any) {
        next(error);
    }
};

const verifyPermission = (permissions: Array<string>) => {
    return (req: Request, res: Response, next: NextFunction) => {
        try {
            const user: USER_TYPE = res.locals.user;
            const rule = user.payment.roles;
            if (permissions.includes(rule)) {
                next();
            } else {
                throw new BadRequestError(
                    `You don't have permission to access this api !!`
                );
            }
        } catch (error) {
            next(error);
        }
    };
};

export { verifyToken, check_lock, verifyPermission };
