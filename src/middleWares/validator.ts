import { check } from 'express-validator';

const create_user_validator = [
    check('email')
        .notEmpty()
        .withMessage('Email is required')
        .isEmail()
        .withMessage('email must be Email'),
    check('username')
        .notEmpty()
        .withMessage('Username is required')
        .isString()
        .withMessage('username must be String'),
    check('password')
        .notEmpty()
        .withMessage('Password is required')
        .isString()
        .withMessage('password must be String')
        .isLength({ min: 8, max: 20 })
        .withMessage('Length of password from 8 - 20')
];

const create_payment_validator = [
    check('bankName')
        .notEmpty()
        .withMessage('bankName is required')
        .isString()
        .withMessage('bankName must be String'),
    check('name')
        .notEmpty()
        .withMessage('Account Name is required')
        .isString()
        .withMessage('name must be String'),
    check('account')
        .notEmpty()
        .withMessage('Number Card is required')
        .isNumeric()
        .withMessage('account must be number')
];

const create_deposit_withdraw_validator = [
    check('amount')
        .notEmpty()
        .withMessage('Amount USD is not empty')
        .isNumeric()
        .withMessage('amount must be number'),
    check('method')
        .notEmpty()
        .withMessage('Bank Id is required')
        .isString()
        .withMessage('method must be String')
];

const create_bill_validator = [
    check('price')
        .notEmpty()
        .withMessage('Price coin is required')
        .isNumeric()
        .withMessage('price must be number'),
    check('quantity')
        .notEmpty()
        .withMessage('Quantity coin is required')
        .isNumeric()
        .withMessage('Quantity must be number'),
    check('symbol')
        .notEmpty()
        .withMessage('Symbol coin is required')
        .isString()
        .withMessage('symbol must be String')
        .contains('USDT')
        .withMessage('symbol must contains USDT')
];

const create_coin_inactive = [
    check('name')
        .notEmpty()
        .withMessage('Name of coin is required')
        .isString()
        .withMessage('Name must be String'),
    check('symbol')
        .notEmpty()
        .withMessage('Symbol of coin is required')
        .isString()
        .withMessage('Symbol must be String')
        .contains('USDT')
        .withMessage('Symbol must be contain USDT'),
    check('fullName')
        .notEmpty()
        .withMessage('fullName of coin is required')
        .isString()
        .withMessage('fullName must be String')
];

const create_rank_user = [
    check('fee')
        .notEmpty()
        .withMessage('Fee is required')
        .isNumeric()
        .withMessage('Fee must be number'),
    check('rank')
        .notEmpty()
        .withMessage('Rank is required')
        .isIn(['STANDARD', 'DEMO', 'PRO', 'VIP'])
        .withMessage('Rank must be in STANDARD, DEMO, PRO, VIP')
];

export {
    create_bill_validator,
    create_deposit_withdraw_validator,
    create_user_validator,
    create_payment_validator,
    create_coin_inactive,
    create_rank_user
};
