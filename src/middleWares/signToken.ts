/* eslint-disable @typescript-eslint/no-explicit-any */
import jwt from 'jsonwebtoken';

const signAccessToken = (userId: any) => {
    const accessToken = jwt.sign(
        { uid: userId },
        `${process.env.ACCESS_TOKEN_JWT_SECRET}`,
        {
            expiresIn: '30m'
        }
    );
    return accessToken;
};

const signRefreshToken = (userId: any) => {
    const refreshToken = jwt.sign(
        { uid: userId },
        `${process.env.REFRESH_TOKEN_JWT_SECRET}`,
        {
            expiresIn: '200d'
        }
    );
    return refreshToken;
};

export { signAccessToken, signRefreshToken };
