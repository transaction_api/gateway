import cluster from 'node:cluster';
import { availableParallelism } from 'node:os';
import process from 'node:process';
import { Express } from 'express';
import routes from '../routes/init.route';


export const parallel = (app: Express) => {
    if (cluster.isPrimary) {
        const numCPUs = availableParallelism() -1;
        console.log(`Master ${process.pid} is running`);

        for (let i = 0; i < numCPUs; i++) {
            cluster.fork();
        }

        cluster.on('exit', (worker, code, signal) => {
            console.log(`worker ${worker.process.pid} died`);
        });
    } else {
        routes(app);
        const port = process.env.PORT || 3000;
        app.listen(port, () => {
            console.log(`Worker ${process.pid} started and listening on port ${port}`);
        });
    }
}
