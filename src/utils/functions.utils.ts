import fs from 'fs';

const saltBcrypt = 10;

const precisionRound = (number: number) => {
    const precision = 10;
    const factor = Math.pow(10, precision);
    return Math.round(number * factor) / factor;
};

const restore_image_base_64 = async (
    imageBuffer: string,
    fileName: string,
    where: string
) => {
    const base64Data = imageBuffer.replace(/^data:image\/\w+;base64,/, '');
    const buffer = Buffer.from(base64Data, 'base64');
    const path = `./uploads/${where}/${fileName}`;
    fs.writeFile(path, buffer, (err) => {
        if (err) throw err;
        return 'OK';
    });
};

const rename_file = async (oldPath: string, newPath: string) => {
    await fs.renameSync(oldPath, newPath);
    return 'Rename file successfully';
};

const formatUSD = (number: number) => {
    return new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'USD'
        // notation: 'compact', // compact, short, long - rút gọn
        // compactDisplay: 'short'  ,
    }).format(number);
};

const formatVND = (number: number) => {
    return new Intl.NumberFormat('vi-VN', {
        style: 'currency',
        currency: 'VND'
    }).format(number);
};

const generatePassword = async (length: number) => {
    const charset =
        'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    let password = '';
    for (let i = 0; i < length; i++) {
        const randomIndex = Math.floor(Math.random() * charset.length);
        password += charset[randomIndex];
    }
    return password;
};

export {
    saltBcrypt,
    precisionRound,
    restore_image_base_64,
    formatUSD,
    formatVND,
    rename_file,
    generatePassword
};
