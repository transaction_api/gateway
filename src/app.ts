require('dotenv').config();
import express from 'express';
import cors from 'cors';
import moment = require('moment');
import helmet from 'helmet';
import morgan = require('morgan');
import rfs = require('rotating-file-stream');
import Path from 'path';
import compression = require('compression');
import { Request, Response } from 'express';

const app = express();
const date = moment().format('MMM Do YY');

const accessLogStream = rfs.createStream(`${date}_access.log`, {
    interval: '1d', // rotate daily
    path: Path.join(__dirname, '../logs/morgan')
});

app.use(express.json({ limit: '10mb' }));
app.use(express.urlencoded({ extended: true, limit: '50mb' }));
app.use(
    cors({
        origin: true,
        credentials: true
    })
);
app.use(express.static('uploads'));
app.use(helmet());
app.use(
    morgan(
        ':method :url :status :res[content-length] - :response-time ms :referrer :user-agent',
        {
            stream: accessLogStream
        }
    )
);
app.use(
    compression({
        level: 6,
        threshold: 100 * 1000,
        filter: (req: Request, res: Response) => {
            if (req.headers['x-no-compression']) {
                // don't compress responses with this request header
                return false;
            }
            // fallback to standard filter function
            return compression.filter(req, res);
        }
    })
);


import './databases/init.redis';
import './databases/init.mongodb';
import './databases/init.sequelize';
import './utils/create_folder';
import './utils/Sequelize.utils';
import { parallel } from './utils/parralel';

moment.locale('vi');

parallel(app)