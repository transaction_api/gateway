import { createClient } from 'redis';

// const secret = { host: '1270.0.0.1', port: 6379, password: '' };

const redisClient = createClient({
    url: `${process.env.REDIS_URL}`,
    password: 'admin123'
});

redisClient
    .connect()
    .then(() => console.log('Connected to redis successfully'))
    .catch((err) => console.log(err));

export default redisClient;
