export enum CONTRACT_ENUM_STATUS {
    'Pending' = 'Pending',
    'Confirmed' = 'Confirmed',
    'Completed' = 'Completed',
    'Canceled' = 'Canceled'
}

export enum CONTRACT_ENUM_STATUS_TYPE {
    'USD' = 'USD',
    'AGRICULTURE' = 'AGRICULTURE'
}
