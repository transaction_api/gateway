export enum WITHDRAW_ENUM_STATUS {
    'Pending' = 'Pending',
    'Confirmed' = 'Confirmed',
    'Completed' = 'Completed',
    'Canceled' = 'Canceled'
}
