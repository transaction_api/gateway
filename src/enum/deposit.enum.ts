export enum DEPOSIT_ENUM_STATUS {
    'Pending' = 'Pending',
    'Completed' = 'Completed',
    'Canceled' = 'Canceled'
}
